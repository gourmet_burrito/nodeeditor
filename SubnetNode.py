from PyQt5 import QtCore, QtGui
from PyQt5.Qt import Qt

from NetworkItem import NetworkItem
from Plug import Plug


class SubnetNode(NetworkItem):
    def __init__(self, **kwargs):
        super(SubnetNode, self).__init__(**kwargs)
        self.inputs  = []
        self.outputs = []

        self.offset = 20 
        self.incr   = 15

        self._label = '' 
        self._image = QtGui.QPixmap('')
        
        self._shape = QtGui.QPainterPath()
        self._bbox  = QtCore.QRect()
        self.reshape()

    def _setLabel(self, text):
        self._label = text
        return

    def _addInput(self, plug):
        accum  = self.offset
        accum += self.incr * len(self.inputs)

        x = self.shape().boundingRect().left() - 5
        plug.setPos(QtCore.QPointF(x, accum))
        plug.setParentItem(self)

        text  = plug.name #QtCore.QString(plug.name)   XXX python2.7

        self.inputs.append(plug)
        
    def _addOutput(self, plug):
        accum  = self.offset
        accum += self.incr * (len(self.outputs) + len(self.inputs))
        
        x = self.shape().boundingRect().right() - 5
        plug.setPos(QtCore.QPointF(x, accum))
        plug.setParentItem(self)

        text  = 'output %s' % len(self.outputs)

        self.outputs.append(plug)

    def getInput(self, index):
        return self.inputs[index]

    def getOutput(self, index):
        return self.outputs[index]

    def boundingRect(self):
        return self._bbox

    def reshape(self):
        inputs  = len(self.inputs)
        outputs = len(self.outputs)

        main_shape = QtGui.QPainterPath()
        width = 100 #max( 100, self.offset + 10 + self._label.geometry().width() )

        main_shape.addRoundedRect(0, 0, width, self.offset+(self.incr*max(inputs+outputs,1)), 5, 5)

        self._shape = main_shape
        self._bbox = main_shape.boundingRect()

    def shape(self):
        return self._shape

    def paint(self, painter, option, widget):
        super(SubnetNode, self).paint(painter, option, widget)

        painter.setClipRect(option.exposedRect)

        penClr = QtCore.Qt.darkGray
        if self.selected:
            penClr = QtCore.Qt.white

        col1 = QtGui.QColor(65,135,170)
        col1 = QtGui.QColor(35,105,140)

        col1.setAlpha(200)

        col2 = QtGui.QColor(25,25,25)
        col2.setAlpha(200)

        gradient = QtGui.QLinearGradient(50, self.offset-10, 50, self.offset)
        gradient.setColorAt(0, col1)
        gradient.setColorAt(1, col2)

        painter.setBrush(QtGui.QBrush(gradient))
        painter.setPen(QtGui.QPen(penClr))
        painter.drawPath( self.shape() )

        painter.setPen( QtGui.QPen(QtGui.QColor(200, 200, 200)) )

        # the image of the ndoe
        painter.drawPixmap(QtCore.QRect(3,3,self.offset, self.offset), self._image)

        # the label text for the ndoe
        x = self.offset+10
        y = self.shape().boundingRect().top()+(self.offset/2)
        painter.drawText(x,y, self._label)

        for plug in self.inputs:
            text = plug.name
            plug_rect = plug.boundingRect() # QRect
            plug_pos = plug.pos() # QPoint
            x,y = plug_pos.x()+plug_rect.right(), plug_pos.y()+plug_rect.bottom()
            painter.drawText(x,y, text)

        font_size = 9 # XXX Qt uses a default 9px font size
        for plug in self.outputs:
            text = plug.name
            label_rect_width = font_size * len(text)
            plug_rect = plug.boundingRect() # QRect
            plug_pos = plug.pos() # QPoint
            x,y = label_rect_width+plug_rect.width(), plug_pos.y()+plug_rect.bottom()
            painter.drawText(x,y, text)

    def mouseMoveEvent(self, event):    
        super(SubnetNode, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):    
        super(SubnetNode, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):    
        super(SubnetNode, self).mouseReleaseEvent(event)

    @classmethod
    def createFromItems(cls, items):
        node = cls(ID="subnetwork_node")
        node._label = "Subnetwork"

        color = Plug.Color.DATA
        
        from Node import Node
        for item in items:
            if not isinstance(item, Node):
                continue
            for input_ in item.inputs:
                connected_nodes = input_.connectedTo()
                for connected_node in connected_nodes:
                    if connected_node not in items:
                         plug = Plug(parent=node, name=input_.name, plug_type=Plug.Type.INPUT, color=color)
                         node._addInput(plug)
            node.reshape()
            
            for output in item.outputs:
                connected_nodes = output.connectedTo()
                for connected_node in connected_nodes:
                    if connected_node not in items:
                         plug = Plug(parent=node, name=output.name, plug_type=Plug.Type.OUTPUT, color=color)

                         node._addOutput(plug)
            node.reshape()
        return node
