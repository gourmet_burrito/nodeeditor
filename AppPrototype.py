import sys

from PyQt5 import QtCore, QtGui, QtOpenGL, QtWidgets
from PyQt5.Qt import Qt

from NetworkScene import NetworkScene
from NetworkScene import QTNetworkScene
from NetworkScene import AddNodeCommand
from NetworkScene import DeleteItemsCommand
from NetworkScene import MoveItemsCommand
from NetworkScene import AddEdgeCommand
from NetworkScene import GroupCommand
from NetworkView import NetworkView
from InteractiveNetworkView import InteractiveNetworkView

from MinimapWidget import MinimapWidget
from ParametersWidget import ParametersWidget
from Outliner import Outliner

#import darkorange

'''
This file is the entry type to a prototype application that 
uses the Network framework classes. Part of this prototype 
app are the MinimapWidget and ParameterWidget - ie, they arent 
part of the framework

I think the App should also provide a Scene as the model to use
by the the Network View. Maybe I need to make one as part of
the module framework and expect applications to use it, or
subclass from it

performance considerations:
 - InteractiveNetwork.mouseMoveEvent() seems slow
 - Plugs slows down the scene
 - Probably Edges too but not confirmed
'''


class ViewChangedPackage(object):
    def __init__(self, **kwargs):
        pass

class AppPrototypeView(InteractiveNetworkView):

    viewChangedSignal = QtCore.pyqtSignal(ViewChangedPackage)

    def __init__(self):
        super(AppPrototypeView, self).__init__()

        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)

        viewport = QtOpenGL.QGLWidget()
        assert(viewport.isValid())
        self.setViewport(viewport)

        #self.setRenderHint(QtGui.QPainter.Antialiasing, False)
        #self.setOptimizationFlags(self.DontSavePainterState)
        #self.setViewportUpdateMode(self.SmartViewportUpdate)

    def setScene(self, scene):
        super(AppPrototypeView, self).setScene(scene)

    def nodesGroupedSlot(self, package):
        items = package.items
        group = package.group
        scene = self.scene()
        groupCommand = GroupCommand(group=group, items=items, scene=scene)
        undoStack = scene.undoStack
        undoStack.push(groupCommand)

    def nodeAddedSlot(self, package):
        node = package.node
        scene = self.scene()
        addNodeCommand = AddNodeCommand(node=node, scene=scene)
        undoStack = scene.undoStack
        undoStack.push(addNodeCommand)

    def itemsDeletedSlot(self, package):
        items = package.items
        scene = self.scene()
        deleteItemsCommand = DeleteItemsCommand(items=items, scene=scene)
        undoStack = scene.undoStack
        undoStack.push(deleteItemsCommand)

    def edgeAddedSlot(self, package):
        edge = package.edge
        source = package.source
        target = package.target
        scene = self.scene()
        addEdgeCommand = AddEdgeCommand(edge=edge, source=source, target=target, scene=scene)
        undoStack = scene.undoStack
        undoStack.push(addEdgeCommand)

    def itemsMovedSlot(self, package):
        moved_items = package.moved_items
        scene = self.scene()
        moveItemsCommand = MoveItemsCommand(moved_items=moved_items)
        undoStack = scene.undoStack
        undoStack.push(moveItemsCommand)

    def viewportEvent(self, event):
        self.viewChangedSignal.emit(ViewChangedPackage())
        return super(AppPrototypeView, self).viewportEvent(event)

class AppPrototype(QtWidgets.QMainWindow):
    def __init__(self, **kwargs):
        super(AppPrototype, self).__init__(**kwargs)        

        import os
        self.stylesheets = ['./css/%s' %f for f in os.listdir('./css') if f.endswith('stylesheet')]
        self.styleidx = 0
        with open(self.stylesheets[self.styleidx],"r") as fh:
            self.setStyleSheet(fh.read())

        import NetworkEngine
        netengine = NetworkEngine.ArnoldNetworkEngine()
        scene = NetworkScene(engine=netengine)
        scene.setSceneRect(0, 0, 5000, 5000)

        self.appPrototypeView = AppPrototypeView()
        self.appPrototypeView.setScene(scene)

        self.minimap = MinimapWidget()
        self.minimap.minimapView.setScene(scene)
        self.minimap.minimapView.refView = self.appPrototypeView

        self.outliner = Outliner()
        # XXX This creates the error message on exit:
        #        QObject::startTimer: QTimer can only be used with threads started with QThread
        self.outliner.setModel(scene.model)

        self.parameters = ParametersWidget()
        self.parameters.parameterChangedSignal.connect(self.update_node)

        self.undoView = QtWidgets.QUndoView()
        # XXX This creates the error message on exit:
        #        QObject::startTimer: QTimer can only be used with threads started with QThread
        self.undoView.setStack(scene.undoStack)

        self.appPrototypeView.nodeSelectedSignal.connect(self.update_parameter_view)
        # XXX this signal seems to occasionally cause, what appears like, an infinite loop
        #     resulting in the application hanging
        self.appPrototypeView.viewChangedSignal.connect(self.update_minimap_view)        


        # TODO: When the dock widgets are torn off from their area's
        #       you can't keep them floating above the main app, they
        #       keep trying to redock (you have to drag them off away
        #       from your app on the desktop.
        #       Thinking about creating a custom TitleBar widget that
        #       has a button to enable/disable undocking by setitng
        #       the NoDockWidgetArea flag. Can set the title bar with
        #       dock.setTitleBarWidget(...)
        dock = QtWidgets.QDockWidget("Viewer", self)
        dock.setObjectName("viewer")
        # XXX I dont know why I had to make this a QScrollArea for the 
        #     border focus defined in the stylesheet to work. All other 
        #     dock windows work fine but this one doesnt when I used regular
        #     QtGui.QWidget() before using QScrollArea(). Maybe its my stylesheet
        dock.setWidget(QtWidgets.QScrollArea())
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dock)

        dock = QtWidgets.QDockWidget("Graph", self)
        dock.setObjectName("graph")
        dock.setWidget(self.appPrototypeView)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dock)

        dock = QtWidgets.QDockWidget("Parameters", self)
        dock.setObjectName("parameters")
        dock.setWidget(self.parameters)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)

        dock = QtWidgets.QDockWidget("Minimap", self)
        dock.setObjectName("minimap")
        dock.setWidget(self.minimap)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)

        dock = QtWidgets.QDockWidget("History", self)
        dock.setObjectName("history")
        # XXX this creates the error message on exit:
        #        qobject::starttimer: qtimer can only be used with threads started with qthread
        dock.setWidget(self.undoView)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)

        dock = QtWidgets.QDockWidget("Outliner", self)
        dock.setObjectName("outliner")
        dock.setWidget(self.outliner)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dock)

        self.setDockOptions(self.AllowTabbedDocks|self.AllowNestedDocks)
          

    # TODO this is temporray implementation
    def keyPressEvent(self, event):
        import os
        if event.key() >= Qt.Key_0 and event.key() <= Qt.Key_9:
            s_file = os.path.join(os.getenv('APPDATA'), 'AppPrototypeSettings_%d.ini')
            space = event.key() - Qt.Key_0
            s_file = s_file % space

            if event.modifiers() & Qt.ControlModifier:
                settings = QtCore.QSettings(s_file, QtCore.QSettings.IniFormat)
                settings.setValue("geometry", self.saveGeometry())
                for child in self.findChildren(QtGui.QDockWidget):
                    settings.setValue("state/%s" % child.objectName(), self.saveState())
                print("layout prefs saved to: %s" % s_file)
            else:
                print("setting layout prefs from: %s" % s_file)
                settings = QtCore.QSettings(s_file, QtCore.QSettings.IniFormat)
                self.restoreGeometry(settings.value("geometry")) #.toByteArray())
                for child in self.findChildren(QtGui.QDockWidget):
                    self.restoreState(settings.value("state/%s" % child.objectName())) #.toByteArray())

        if (event.key() == Qt.Key_BracketLeft) or (event.key() == Qt.Key_BracketRight):
            if event.key() == Qt.Key_BracketLeft:
                self.styleidx = (self.styleidx - 1) % len(self.stylesheets)
            else:
                self.styleidx = (self.styleidx + 1) % len(self.stylesheets)

            sshFile = self.stylesheets[self.styleidx]
            print ("Setting style to", sshFile)
            with open(sshFile,"r") as fh:
                self.setStyleSheet(fh.read())


        if (event.key() == Qt.Key_S) and (event.modifiers() & Qt.ControlModifier):
            s_file = os.path.join(os.getenv('APPDATA'), 'scene.ass')
            print("Saving scene to", s_file)
            engine = self.appPrototypeView.scene().engine
            engine.to_file(s_file)
            print("Saving complete")

    def update_node(self, package):
        print("a node parameter was updated")
        value = package.value
        node = package.node_name
        param_name = package.parameter_name

        engine = self.appPrototypeView.scene().engine
        engine.setParam(node, param_name, value)

    def update_parameter_view(self, package):
        self.parameters.setFromDict(package.nodedata)

    def update_minimap_view(self, package):
        self.minimap.fit()

    def populate(self, count):
        from Node import Node
        import random
        scene = self.appPrototypeView.scene()
        nodeDict = scene.get_node("multiply_node")

        for i in range(count):
            x,y = random.randint(1,1500), random.randint(-700,700)
            node = Node.createFromDict(nodeId="multiply_node", nodeDict=nodeDict)
            node.setFlag(QtGui.QGraphicsItem.ItemIsMovable)                
            node.setPos(QtCore.QPointF(2000+x, 1500+y))

            command = AddNodeCommand(node=node, scene=scene)
            scene.undoStack.push(command)

def main():
    #QtWidgets.QApplication.setGraphicsSystem("raster")
    app = QtWidgets.QApplication(sys.argv)
    
    app_prototype = AppPrototype()
    app_prototype.show()
    #app_prototype.populate(0)

    sys.exit(app.exec_())

if __name__ == "__main__":
    if len(sys.argv) == 1:
        main()
    else:
        import cProfile
        cProfile.run('main()')
