things to do:
- finish work on node grouping (UI elements for incoming/outgoing connections are still incomplete)
- nodes with "event flow" flow plug (currently only have "data flow"). Should be controlable somehow
  (for flows that are "explicit" vs "implicit")
- copy and paste of selected graph items
- user input/key actions should be pulled out of InteractiveNetowrkView. 
