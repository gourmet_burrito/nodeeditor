from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from itertools import count
iid = count()

class NetworkItem(QtWidgets.QGraphicsItem):
    class Color:
        INTEGER = (100,110,255)
        FLOAT   = (160, 150, 255)
        STRING  = (60, 225, 110)
        DATA    = (255,120,150)
        UNKNOWN = (128,128,128)

        class BorderColor:
            DEFAULT = QtGui.QColor(0,0,0)
            SELECTED = QtGui.QColor(255,255,255)        

    def __init__(self, *args, **kwargs):
        self.id_ = kwargs.pop("ID", self.__class__.__name__)
        super(NetworkItem, self).__init__(*args, **kwargs)
        #self.id_ = "%s%s" % (id_, next(iid))

        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemNegativeZStacksBehindParent)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        #self.setCacheMode(QtGui.QGraphicsItem.ItemCoordinateCache)

        self.setAcceptHoverEvents(True)
        self.setZValue(1)

        self.selected = False
        self.hover    = False

        self.childrenNodes = []
        self.parentSubnet = None
        self.row = self.id_

        if False:
            self.drop_shadow = QtGui.QGraphicsDropShadowEffect()        
            self.drop_shadow.setBlurRadius(25)
            self.drop_shadow.setOffset(0)
            self.drop_shadow.setColor( NetworkItem.Color.BorderColor.DEFAULT )

            self.setGraphicsEffect( self.drop_shadow )

    def paint(self, painter, option, widget):
        if self.hover:
            pen = QtGui.QPen( QtCore.Qt.yellow )
            pen.setWidthF(1.0)
            painter.setPen(pen)
            painter.drawRect(self.boundingRect())

        #return super(NetworkItem, self).paint(painter, option, widget)

    def adjust(self):
        return

    def sceneEvent(self, event):
        return super(NetworkItem, self).sceneEvent(event)

    def hoverEnterEvent(self, event):
        super(NetworkItem, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        super(NetworkItem, self).hoverLeaveEvent(event)

    def mouseMoveEvent(self, event):
        super(NetworkItem, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        super(NetworkItem, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):    
        super(NetworkItem, self).mouseReleaseEvent(event)

    def setSelected(self, state):
        self.selected = state
        self.update()

