from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.Qt import Qt

from Edge import Edge
from Node import Node
from Plug import Plug
from Group import Group
from SubnetNode import SubnetNode

# The undoStack is created here globally because when a UndoView
# (ex. the one in AppPrototype.py) references an instance of the undostack
# created from within IneractiveBetworkView, you get a QTimer error upon
# application exit
#undoStack = QtGui.QUndoStack()

MOVE_ITEMS_ID = 1
ADD_NODE_ID   = 2
ADD_EDGE_ID   = 3


class MoveItemsCommand(QtWidgets.QUndoCommand):
    # TODO: Use of the self.first flag is sooo sooo hack
    #       need to come up with a better implementation

    def __init__(self, **kwargs):
        #self.items = kwargs.pop('items')
        #self.start = kwargs.pop('start')
        #self.end = kwargs.pop('end')
        #self.scene = kwargs.pop('scene')

        self.moved_items = kwargs.pop("moved_items")
        super(MoveItemsCommand, self).__init__(**kwargs)

        text = "Moved Items"
        self.setText(text)

    def id(self):
        return MOVE_ITEMS_ID

    def mergeWith(self, other):
        return False

    def redo(self):
        for item, positions in self.moved_items.items():
            from_, to = positions
            item.setPos(to)
        return

    def undo(self):
        for item, positions in self.moved_items.items():
            from_, to = positions
            item.setPos(from_)
        return

class AddEdgeCommand(QtWidgets.QUndoCommand):
    def __init__(self, **kwargs):
        self.edge = kwargs.pop('edge')
        self.source = kwargs.pop('source')
        self.target = kwargs.pop('target')
        self.scene = kwargs.pop('scene')
        super(AddEdgeCommand, self).__init__(**kwargs)

        text = "Connected Nodes"
        self.setText(text)

    def redo(self):
        self.edge.setSource(self.source)
        self.edge.setTarget(self.target)
        self.scene.addItem(self.edge)
        self.edge.update()

    def undo(self):
        self.source.detachEdge(self.edge)
        self.target.detachEdge(self.edge)
        self.scene.removeItem(self.edge)

class GroupCommand(QtWidgets.QUndoCommand):
    def __init__(self, **kwargs):
        self.group = kwargs.pop('group')
        self.items = kwargs.pop('items')
        self.scene = kwargs.pop('scene')
        super(GroupCommand, self).__init__(**kwargs)

        text = "Grouped Nodes"
        self.setText(text)

        # determine the edges which are connected to nodes within the 
        # selected items list on both ends
        self.inclusive_edges = set()
        for item in self.items:
            if not isinstance(item, Node):
                continue
            for plug in item.inputs + item.outputs:
                edges = [edge for edge in plug.edges if \
                                  edge.getSourceNode() in self.items and\
                                  edge.getTargetNode() in self.items]

                self.inclusive_edges = self.inclusive_edges.union(set(edges))
        self.inclusive_edges = list(self.inclusive_edges)

        for item in self.items+self.inclusive_edges:
            self.group.groupItem(item)

        z = -1
        for item in self.group.grouped_items:                
            if item.zValue() <= z:
                z = item.zValue()-1
        self.group.setZValue(z)

    def redo(self):
        self.scene.addItem(self.group)

        for item in self.items:
            if not isinstance(item, Node):
                continue

    def undo(self):
        for item in self.items:
            if not isinstance(item, Node):
                continue

        self.scene.removeItem(self.group)

class AddNodeCommand(QtWidgets.QUndoCommand):
    def __init__(self, **kwargs):
        self.node = kwargs.pop('node')
        self.scene = kwargs.pop('scene')
        super(AddNodeCommand, self).__init__(**kwargs)

        label = self.node._label
        text = "Node Added (%s)" % label
        self.setText(text)

    def redo(self):
        self.scene.addItem(self.node)

    def undo(self):
        self.scene.removeItem(self.node)

class DeleteItemsCommand(QtWidgets.QUndoCommand):
    def __init__(self, **kwargs):
        self.selected_items = kwargs.pop('items')
        self.scene = kwargs.pop('scene')
        super(DeleteItemsCommand, self).__init__(**kwargs)

        text = "Items Deleted"
        self.setText(text)

        self.deleted_items = []

    def redo(self):
        # TODO: this function is implemented pretty badly. 
        #       tons of re-looping over items.

        # remove the list of items
        for item in self.selected_items:
            if isinstance(item, Node) or \
               isinstance(item, Edge):

                self.deleted_items.append(item)
                self.scene.removeItem(item)

        # if a group is being deleted, ungroup it's containing items
        # then delete the group
        for item in self.selected_items:
            if isinstance(item, Group):
                for child in item.childItems():
                #for child in item.grouped_items:
                    if isinstance(child, Node) or \
                       isinstance(child, Edge):
                        pos = child.pos().toPoint()
                        child.setParentItem(None)
                        pos = item.mapToScene(pos)
                        child.setPos(pos)
                        child.setZValue(-1*child.zValue())
                        #child.setVisible(True)
                self.deleted_items.append(item)
                self.scene.removeItem(item)

        # from the items that are left in the scene, remove the dangling edges
        items = self.scene.items()
        for item in items:
            if isinstance(item, Edge):
                if (item.source not in items) or \
                   (item.target not in items):
                    self.deleted_items.append(item)
                    self.scene.removeItem(item)

        # from the plugs left in the scene, detach the edges that were removed 
        # from the scene
        edges = set()
        plugs = [plug for plug in self.scene.items() if isinstance(plug, Plug)]
        for plug in plugs:
            edges.update(set(plug.edges))

        for edge in edges:
            if edge not in self.scene.items():
                edge.getSourcePlug().detachEdge(edge)
                edge.getTargetPlug().detachEdge(edge)

                self.scene.engine.disconnect(edge.getTargetPlug().parent.label(), edge.getTargetPlug().name)

        
    def undo(self):
        for item in self.deleted_items:
            self.scene.addItem(item)

        for item in self.deleted_items:
            if isinstance(item, Edge):
                source = item.getSourcePlug()
                target = item.getTargetPlug()
                source.attachEdge(item)
                target.attachEdge(item)

        self.deleted_items = []



class NetworkModel(QtCore.QAbstractItemModel):
    def __init__(self, scene):
        super(NetworkModel, self).__init__()
        self.scene = scene

        self.rootItem = SubnetNode()
        self.rootItem.setVisible(False)

        #self.rowsAboutToBeInserted.connect(self.sweet)

    # Returns the number of rows under the given parent.
    # When the parent is valid it means that rowCount is returning 
    # the number of children of parent.
    #Note: When implementing a table based model, rowCount() 
    # should return 0 when the parent is valid.
    def rowCount(self, parent):
        if parent.column() > 1:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return len(parentItem.childrenNodes)

    # Returns the number of columns for the children of the given parent.       
    def columnCount(self, parent):
        return 1

    # Returns the parent of the model item with the given index.
    # If the item has no parent, an invalid QModelIndex is returned.
    def parent(self, child):
        if not child.isValid():
            return QtCore.QModelIndex()

        childNode = child.internalPointer()
        parentSubnet = childNode.parentSubnet

        if not parentSubnet or parentSubnet == self.rootItem:
            return QtCore.QModelIndex()

        row = 0
        grandparentSubnet = parentSubnet.parentSubnet
        if grandparentSubnet:
            row = grandparentSubnet.childrenNodes.index(parentSubnet)

        return self.createIndex(row, 0, parentSubnet)

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != QtCore.Qt.DisplayRole:
            return None

        item = index.internalPointer()
        name = item.id_
        return name

    #Returns the index of the item in the model specified by the given row, column and parent index.
    def index(self, row, column, parentIndex):
        if not self.hasIndex(row, column, parentIndex):
            return QtCore.QModelIndex()

        if not parentIndex.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        try:
            childItem = parentItem.childrenNodes[row]
            return self.createIndex(row, column, childItem)
        except:

            return QtCore.QModelIndex()

class NetworkScene(QtWidgets.QGraphicsScene):
    def __init__(self, engine=None):
        super(NetworkScene, self).__init__()
        self.engine = engine
        self.undoStack = QtWidgets.QUndoStack()
        self.model = NetworkModel(self)

#    def list_nodes(self):
#        return self.engine.nodes
#
#    def get_node(self, id_):
#        nodeDict = self.engine.create_node(id_)
#        node = Node.createFromDict(nodeId=id_, nodeDict=nodeDict)
#        return node    

    def addItem(self, item):
        super(NetworkScene, self).addItem(item)
        # this block should be in the ItemModel
        if isinstance(item, Group):
            return
        if isinstance(item, Node):
            first = last = len(self.model.rootItem.childrenNodes)
            self.model.beginInsertRows(QtCore.QModelIndex(), first, last)
            self.model.rootItem.childrenNodes.append(item)
            self.model.endInsertRows()
    
    def removeItem(self, item):
        # this block should be in the ItemModel
        if item in self.model.rootItem.childrenNodes:
            first = last = self.model.rootItem.childrenNodes.index(item)
            self.model.beginRemoveRows(QtCore.QModelIndex(), first, last)
            self.model.rootItem.childrenNodes.remove(item)
            self.model.endRemoveRows()

        super(NetworkScene, self).removeItem(item)

class QTNetworkScene(NetworkScene):
    def __init__(self, engine=None):
        super(QTNetworkScene, self).__init__()
        self.engine = engine
        self.undoStack = QtGui.QUndoStack()        

    def list_nodes(self):
        nodes = sorted(self.engine.nodes)
        return nodes

    def get_node(self, id_):
        qtObj = self.engine.node(id_)
        node = Node.createFromQTObj(nodeId=id_, nodeObj=qtObj)
        return node
