from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from NetworkItem import NetworkItem
from Node import Node
from Plug import Plug
from Edge import Edge

class Control(NetworkItem):

    # This is subclass is necessary in order for a QGraphicsItem to 
    # emit a signal
    class ControlSignal(QtCore.QObject):
        clickedSignal = QtCore.pyqtSignal()

    def __init__(self, **kwargs):
        super(Control, self).__init__(**kwargs)

        self._shape = QtGui.QPainterPath()
        self._bbox  = QtCore.QRect()

        self.isExpanded = True 
        self.reshape()
        self.signal_object = Control.ControlSignal()
        self.clickedSignal = self.signal_object.clickedSignal

    def boundingRect(self):
        return self._bbox

    def reshape(self):
        main_shape = QtGui.QPainterPath()
        if self.isExpanded:
            main_shape.moveTo(2,5)
            main_shape.lineTo(8,5)
        else:
            main_shape.addRect(2,2,6,6)

        self._shape = main_shape
        self._bbox = QtCore.QRectF(0,0,20,20)

    def shape(self):
        shape = QtGui.QPainterPath()
        shape.addRect(self._bbox)
        return shape

    def paint(self, painter, option, widget):
        super(Control, self).paint(painter, option, widget)

        painter.setClipRect(option.exposedRect)

        penClr = QtGui.QColor(200, 200, 200)
        pen = QtGui.QPen( penClr )
        pen.setWidthF(1.0)

        painter.setPen( pen )
        painter.drawPath( self._shape )

    def mouseReleaseEvent(self, event):    
        super(Control, self).mouseReleaseEvent(event)
        self.isExpanded = not self.isExpanded
        self.reshape()
        self.clickedSignal.emit()

class Group(Node):
    def __init__(self, **kwargs):
        super(Group, self).__init__(**kwargs)

        self._setImage("./images/group.png")
        self._setLabel("Item Group")
        self._color = QtGui.QColor(50,50,50,128)
        self.offset = 50 
        self.incr   = 20

        self.width = 100
        self.height = 100

        self.start = QtCore.QPointF()
        self.collapsed_spot = QtCore.QPointF()
        
        self.group_control = Control(parent=self)
        self.group_control.clickedSignal.connect(self.group_control_slot)

        self.grouped_items = []

        self.incoming = []
        self.outgoing = []
        self.inclusive_edges = []

        self.movers = []


    def groupItem(self, item):
        #self.grouped_items.append(item)
        #self.adjust()
        #self.setPos(self.start)
        #pos = item.pos().toPoint()
        #item.setParentItem(self)
        #pos = self.mapFromScene(pos)
        #item.setPos(pos)

        #if isinstance(item,Node):
        #    item.nodeMovedSignal.connect(self.groupedNodeMoved)

        #return

        
        self.grouped_items.append(item)
        self.adjust()
        self.setPos(self.start)

        if isinstance(item,Node):
            item.nodeMovedSignal.connect(self.groupedNodeMoved)
            self.movers.append(item.moveBy)

        self.updateInclusiveEdges()

    def updateInclusiveEdges(self):
        self.inclusive_edges = set()
        for item in self.grouped_items:
            if not isinstance(item, Node):
                continue
            for plug in item.inputs + item.outputs:
                edges = [edge for edge in plug.edges if \
                                  edge.getSourceNode() in self.grouped_items and\
                                  edge.getTargetNode() in self.grouped_items]

                self.inclusive_edges = self.inclusive_edges.union(set(edges))
        self.inclusive_edges = list(self.inclusive_edges)
   
    def groupedNodeMoved(self):
        self.adjust()
        self.setPos(self.start)

    def adjust(self):
        if self.group_control.isExpanded:
            rect = QtCore.QRectF()
            for item in self.grouped_items:
                if not isinstance(item, Node) and \
                    not isinstance(item, Group):
                    continue

                r = item.boundingRect().translated(item.pos())
                rect = rect.united(r)

            control_offset = QtCore.QPointF(0,self.group_control.boundingRect().height())

            self.start = rect.topLeft()-QtCore.QPointF(0, self.offset)
            self.width = rect.width()
            self.height = rect.height()+self.offset
            self.prepareGeometryChange()
        else:
            super(Group, self).adjust()
            self.width = self.width+50
            # XXX
            # Below is to override visibility setting from super()s adjust()
            for edge in self.inclusive_edges:
                edge.setVisible(self.group_control.isExpanded)
       
        offset = self.group_control.boundingRect().width()
        x_position = self.width - offset
        self.group_control.setPos(x_position,0) 
        
        for child in self.childItems():
            child.adjust()
                   
    def boundingRect(self):
        width = self.width
        height = self.height

        bbox = QtCore.QRectF(0,0,width, height)
        bbox.adjust(-0.5,-0.5,0.5,0.5)
        return bbox

    def paint(self, painter, option, widget):
        super(Group, self).paint(painter, option, widget)
        return


    def collapse(self):
        passing_in_edges = set()
        passing_out_edges = set()
        for item in self.grouped_items:
            if not isinstance(item, Node):
                continue

            for plug in item.inputs:
                edges = [edge for edge in plug.edges if \
                                    edge.getSourceNode() not in self.grouped_items]
                passing_in_edges = passing_in_edges.union(set(edges))
            self.incoming = [edge.getTargetPlug() for edge in passing_in_edges]

            for plug in item.outputs:
                edges = [edge for edge in plug.edges if \
                                    edge.getTargetNode() not in self.grouped_items]
                passing_out_edges = passing_out_edges.union(set(edges))
            self.outgoing = [edge.getSourcePlug() for edge in passing_out_edges]

        for incoming in self.incoming:
            incoming.name = incoming.parent.id_ + ':' + incoming.name
            self._addInput(incoming)
            if incoming in incoming.parent.inputs:
                incoming.parent.inputs.remove(incoming)
            incoming.adjust()

        for outgoing in self.outgoing:
            outgoing.name = outgoing.parent.id_ + ':' + outgoing.name
            self._addOutput(outgoing)
            if outgoing in outgoing.parent.outputs:
                outgoing.parent.outputs.remove(outgoing)
            outgoing.adjust()

        for edge in passing_in_edges.union(passing_out_edges):
            edge.adjust()

        self.adjust()

    def expand(self):
        for incoming in self.incoming:
            if incoming in self.inputs:
                self.inputs.remove(incoming)
            incoming.name = incoming.name.split(':')[-1]
            incoming.parent._addInput(incoming)
            incoming.parent.adjust()

        for outgoing in self.outgoing:
            if outgoing in self.outputs:
                self.outputs.remove(outgoing)
            outgoing.name = outgoing.name.split(':')[-1]
            outgoing.parent._addOutput(outgoing)                
            outgoing.parent.adjust()
        self.incoming = []
        self.outgoing = []
        self.adjust()
        
    def group_control_slot(self):
        if self.group_control.isExpanded:
            self.expand()
        else:
            self.collapse()
            self.collapsed_spot = self.start

        self.updateInclusiveEdges()

        delta = self.start-self.collapsed_spot             
        for item in self.grouped_items:
            if not isinstance(item, Node) and \
                not isinstance(item, Group):
                continue
            item.moveBy(delta.x(), delta.y())

        for item in self.grouped_items:
            if item in self.incoming+self.outgoing:
                continue
            item.setVisible(self.group_control.isExpanded)
            
        for edge in self.inclusive_edges:
            edge.setVisible(self.group_control.isExpanded)
            edge.adjust()

        self.setPos(self.start)
        self.adjust()

    def itemChange(self, change, value):
        if change == QtWidgets.QGraphicsItem.ItemPositionHasChanged:
            if self.group_control.isExpanded:
                #delta = value.toPyObject()-self.start
                delta = value - self.start
                x,y = delta.x(), delta.y()
                for moveBy in self.movers:
                    moveBy(x,y)

            self.start=value
        return super(Group, self).itemChange(change, value)

    def mouseMoveEvent(self, event):
        super(Group, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        super(Group, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        super(Group, self).mouseReleaseEvent(event)
