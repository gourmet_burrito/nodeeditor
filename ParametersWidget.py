from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt


class MatrixWidget(QtWidgets.QWidget):
    def __init__(self, values=[[0,0,0]]):
        super(MatrixWidget, self).__init__()
        if not any([type(i) == list for i in values]):
            values = [values]

        cols = max([len(i) for i in values if type(i) == list])
        rows = len(values)

        self.dimensions = (rows, cols)

        layout = QtWidgets.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        rows, cols = self.dimensions
        for row in range(rows):
            for col in range(cols):
                field = QtWidgets.QLineEdit(str(values[row][col]))
                layout.addWidget(field, row, col)

        self.setLayout(layout)

class LabeledSlider(QtWidgets.QWidget):
    def __init__(self, value=0):
        super(LabeledSlider, self).__init__()
        self.label = QtWidgets.QLineEdit()
        self.label.setMaximumWidth(50) # XXX Would rather not do this

        if type(value) == float:
            self.label.setValidator(QtGui.QDoubleValidator())
        if type(value) == int:
            self.label.setValidator(QtGui.QIntValidator())
       
        self.slider = QtWidgets.QSlider(Qt.Horizontal)
        try:
            self.slider.setMaximum(10**(len(str(value))+0))
            self.slider.setValue(value)
        except:
            import sys
            self.slider.setMaximum(sys.maxint)
            self.slider.setValue(sys.maxint)
        self.slider.setTickPosition(self.slider.TicksBelow)

        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.slider)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

        self.slider.valueChanged.connect(self.setLabel)
        self.label.textEdited.connect(self.setSlider)

        self.setLabel(value)        

    def setLabel(self, value):
        self.label.setText(str(value))

    def setSlider(self, value):
        try:
            value = int(value)
        except:
            return
            
        self.slider.setValue(value)


def get_widget(value):
    def slider(value):
        value = int(value)

        labeled_slider = LabeledSlider(value)        
        return labeled_slider

    def matrix(value):
        return MatrixWidget(value)

    types_map = {
        int:slider,
        float:slider,
        list: matrix,
    }

    widget = types_map.get(type(value), None)
    if not widget:
        return

    return widget(value)

class ParameterChangedPackage(object):
    def __init__(self, **kwargs):
        pass

class ParametersWidget(QtWidgets.QWidget):
    parameterChangedSignal = QtCore.pyqtSignal(ParameterChangedPackage)
    
    def __init__(self, **kwargs):
        super(ParametersWidget, self).__init__(**kwargs)
        
        self.scroll_area = QtWidgets.QScrollArea(self)
        self.scroll_area.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        layout = QtWidgets.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.addWidget(self.scroll_area)
        self.setLayout(layout)

    def setFromDict(self, itemDict):
        label = itemDict.get('label', '')
        image = itemDict.get('image', '')
        parms = itemDict.get('parameters', {})
        
        self.node_name = label

        central = QtWidgets.QWidget()
        params_layout = QtWidgets.QFormLayout()
        params_layout.setVerticalSpacing(5) # some breathing room
        central.setLayout(params_layout)

        
        imageWidget  = QtWidgets.QLabel()
        imageWidget.setPixmap(QtGui.QPixmap(image).scaledToHeight(64))
        labelWidget  = QtWidgets.QLabel(label)
        header_layout = QtWidgets.QVBoxLayout()
        header_layout.addWidget(imageWidget)
        header_layout.addWidget(labelWidget)
        params_layout.addRow('', header_layout)


        for parm_id, parm_data in sorted(parms.items(), key=lambda k: k[0].lower()):
            param_name = parm_data.get("label", "")
            type_ = parm_data.get("type", None)
            value = parm_data.get("value", 0)

            param_widget = get_widget(value)
            if not param_widget:
                continue

            if hasattr(param_widget, 'slider'):
                param_widget.slider.valueChanged.connect(self.parmChange)
                param_widget.slider.param_name = param_name # XXX Putting the name of the 
                                                            #  param in the widget so that
                                                            #  we know which param was changed
                                                            #  in the signal

            params_layout.addRow(param_name, param_widget)

        self.scroll_area.setWidget(central)
        self.scroll_area.setWidgetResizable(True)

    def parmChange(self, value):
        package = ParameterChangedPackage()
        package.node_name = self.node_name
        package.parameter_name = self.sender().param_name
        package.value = value
        self.parameterChangedSignal.emit(package)



if __name__ == "__main__":

    
    import sys
    qapp = QtGui.QApplication(sys.argv)
    p = ParametersWidget()
    p.show()
    
    p_dict = { 
        'label': 'test',
        'parameters': {
            'wicked': {
                'label': 'wicked sweet',
                'type': 'int',
                'value': 1
            },
            'sweet': {
                'label': 'sweet',
                'type': 'int',
                'value': 10
            },
            'dirtypop': {
                'label': 'sweet',
                'type': 'list',
                'value': [1,0,0]
            }

        }
    }

    p.setFromDict(p_dict)
    qapp.exec_()
        
