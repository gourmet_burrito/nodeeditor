from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from Edge import Edge
from Node import Node
from Plug import Plug

class NetworkView(QtWidgets.QGraphicsView):
    def __init__(self):
        super(NetworkView, self).__init__()

        self.setCacheMode(QtWidgets.QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QtWidgets.QGraphicsView.FullViewportUpdate)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)
        self.setDragMode(QtWidgets.QGraphicsView.RubberBandDrag)
        self.setRenderHint(QtGui.QPainter.Antialiasing)

        self.scale(1.0, 1.0)
        self.setWindowTitle("")

    def drawBackground(self, painter, rect):
        sceneRect = self.sceneRect()

        # draw the background colour. 
        # this seems to be required by python 3 but not python 2
        # this should be made to match the current style options
        painter.fillRect(sceneRect, QtGui.QColor(50, 50, 50))

        bottom_left = sceneRect.bottomLeft()
        top_right   = sceneRect.topRight()

        painter.setPen( QtGui.QColor(75,75,75) )
        for x in range(int(bottom_left.x()), int(top_right.x()), 25):
            painter.drawLine(x, bottom_left.y(), x, top_right.y())

        for y in range(int(top_right.y()), int(bottom_left.y()), 25):
            painter.drawLine(bottom_left.x(), y, top_right.x(), y)

        painter.setPen( QtGui.QColor(25,25,25) )
        for x in range(int(bottom_left.x()), int(top_right.x()), 250):
            painter.drawLine(x, bottom_left.y(), x, top_right.y())

        for y in range(int(top_right.y()), int(bottom_left.y()), 250):
            painter.drawLine(bottom_left.x(), y, top_right.x(), y)


    def scaleView(self, scaleFactor):
        # abs_factor is the factor in absolute value of what the scale is
        abs_factor = self.transform().scale(scaleFactor, scaleFactor).mapRect(QtCore.QRectF(0, 0, 1, 1)).width()

        #if abs_factor < 0.25 or abs_factor > 5.0:
        #    return

        self.scale(scaleFactor, scaleFactor)

    def connect(self, sourceNode, sourcePlugIndex, targetNode, targetPlugIndex):
        source_plug = sourceNode.getOutput(sourcePlugIndex)
        target_plug = targetNode.getInput(targetPlugIndex)

        e = Edge()        
        e.setSource(source_plug)
        e.setTarget(target_plug)

        self.scene().addItem(e)

    #def addItem(self, item):
    #    self.scene().addItem(item)

    #def removeItems(self, items):
    #    for i in items:
    #        self.scene().removeItem(i)


#        # remove the list of items
#        for i in items:
#            if isinstance(i, Node) or isinstance(i, Edge):
#                self.scene().removeItem(i)
#
#        # from the items that are left in the scene, remove the dangling edges
#        items = self.scene().items()
#        for i in items:
#            if isinstance(i, Edge):
#                if (i.source not in items) or (i.target not in items):
#                    self.scene().removeItem(i)


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)

    network = NetworkView()
    network.show()

    sys.exit(app.exec_())
    
