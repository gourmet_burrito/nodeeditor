#
#    ALL EVENTS
# 
#    actionEvent
#    changeEvent
#    childEvent
#    closeEvent
#    contextMenuEvent
#    customEvent
#    dragEnterEvent
#    dragLeaveEvent
#    dragMoveEvent
#    dropEvent
#    enterEvent
#    focusInEvent
#    focusOutEvent
#    hideEvent
#    inputMethodEvent
#    installEventFilter
#    keyPressEvent
#    keyReleaseEvent
#    leaveEvent
#    mouseDoubleClickEvent
#    mouseMoveEvent
#    mousePressEvent
#    mouseReleaseEvent
#    moveEvent
#    paintEvent
#    removeEventFilter
#    resizeEvent
#    showEvent
#    tabletEvent
#    timerEvent
#    viewportEvent
#    wheelEvent
#    winEvent


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from NetworkView import NetworkView
from NetworkItem import NetworkItem

from Edge import Edge
from Node import Node
from Plug import Plug
from Group import Group
from SubnetNode import SubnetNode

class ComputeNodePackage(object):
    def __init__(self, **kwargs):
        self.node = kwargs.get('node', None)

class NodeSelectedPackage(object):
    def __init__(self, **kwargs):
        self.nodedata = kwargs.get('nodedata', None)

class AddNodePackage(object):
    def __init__(self, **kwargs):
        self.node = kwargs.get('node', None)

class GroupNodesPackage(object):
    def __init__(self, **kwargs):
        self.group = kwargs.get('group', None)
        self.items = kwargs.get('items', None)

class AddEdgePackage(object):
    def __init__(self, **kwargs):
        self.edge = kwargs.get('edge', None)
        self.source = kwargs.get('source', None)
        self.target = kwargs.get('target', None)

class DeleteNodePackage(object):
    def __init__(self, **kwargs):
        self.node = kwargs.get('node', None)

class DeleteEdgePackage(object):
    def __init__(self, **kwargs):
        self.edge = kwargs.get('edge', None)

class DeleteItemsPackage(object):
    def __init__(self, **kwargs):
        self.items = kwargs.get('items', None)

class ItemsMovedPackage(object):
    def __init__(self, **kwargs):
        self.items = kwargs.get('items', None)
        self.start = kwargs.get('start', None)
        self.end = kwargs.get('end', None)
        self.moved_items = kwargs.get('moved_items', None)

class InteractiveNetworkView(NetworkView):
    groupNodesSignal    = QtCore.pyqtSignal(GroupNodesPackage)
    addNodeSignal       = QtCore.pyqtSignal(AddNodePackage)
    nodeSelectedSignal  = QtCore.pyqtSignal(NodeSelectedPackage)
    deleteItemsSignal   = QtCore.pyqtSignal(DeleteItemsPackage)
    deleteNodeSignal    = QtCore.pyqtSignal(DeleteNodePackage)
    addEdgeSignal       = QtCore.pyqtSignal(AddEdgePackage)
    deleteEdgeSignal    = QtCore.pyqtSignal(DeleteEdgePackage)
    itemsMovedSignal    = QtCore.pyqtSignal(ItemsMovedPackage)
    computeNodeSignal   = QtCore.pyqtSignal(ComputeNodePackage)

    def __init__(self):
        super(InteractiveNetworkView, self).__init__()
        self.newEdge    = None
        self.mousePos   = QtCore.QPoint()
        self.copy_buffer = []
        self.copy_count = 0

        self.setDragMode(QtWidgets.QGraphicsView.RubberBandDrag)
        self.setMouseTracking(True)

        self.groupNodesSignal.connect(self.nodesGroupedSlot)
        self.addNodeSignal.connect(self.nodeAddedSlot)
        self.addEdgeSignal.connect(self.edgeAddedSlot)
        self.deleteItemsSignal.connect(self.itemsDeletedSlot)
        self.itemsMovedSignal.connect(self.itemsMovedSlot)
    
    def nodesGroupedSlot(self, package):
        pass
       
    def nodeAddedSlot(self, package):
        pass

    def itemsDeletedSlot(self, package):
        pass

    def edgeAddedSlot(self, package):
        pass

    def itemsMovedSlot(self, package):
        pass

    def setScene(self, scene):
        super(InteractiveNetworkView, self).setScene(scene)


    # XXX All of this popup menu code should be part of the calling App
    #     not part of this View/Widget. No other widget (Tree, List, Table, etc)
    #     have built in popup menus
    #     The calling App should create and recieve the right click signal, 
    #     handle the menu, and then uses the menu selection to interact
    #     with the NetworkModel
    #
    def _popupMenu(self):
        scene = self.scene()

        def collapseSelectedNodes():
            position = QtCore.QPointF(self.mapToScene(self.mousePos))
            selectedItems = [item for item in self.scene().selectedItems() 
                                    if isinstance(item, Node)]
            if not selectedItems:
                return
            subnet = SubnetNode.createFromItems(items=selectedItems)
            subnet.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
            subnet.setPos(position)
            self.scene().addItem(subnet)

        def groupSelectedNodes():
            position = QtCore.QPointF(self.mapToScene(self.mousePos))
            selectedItems = [item for item in self.scene().selectedItems()]# if isinstance(item, Node)]
            if not selectedItems:
                return

            group = Group()
            group.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
            
            self.groupNodesSignal.emit(GroupNodesPackage(group=group,items=selectedItems)) 
        
        def createNode(node_name):
            # XXX Bad that Im just creating the node in the engine!
            #     Should send a signal to the App. Let the application
            #     do what it wants with the engine (I would like the App to create a
            #     Command object that creates the node. that way, it is also undoable)

            nodeDict = scene.engine.create(node_name)

            node = Node.createFromDict(nodeId=node_name, nodeDict=nodeDict)
            node.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
            node.setPos(QtCore.QPointF(self.mapToScene(self.mousePos)))
            node.old_position = node.pos()

            self.addNodeSignal.emit(AddNodePackage(node=node))

        def nodesSubMenu(parentMenu, nodesDict):
            from collections import defaultdict
            for key, value in nodesDict.items():
                subMenu = parentMenu.addMenu(key)
                if type(value) == list:
                    node_names = value
                    for node_name in node_names:
                        node_name = str(node_name)
                        subMenu.addAction(node_name, lambda node_name=node_name: createNode(node_name))
                elif type(value) == dict:
                    nodesSubMenu(subMenu, value)
                    

        def styleMenu(menu):
            print("DEBUG: Using styling specific to QMenu")
            menu.setAttribute(Qt.WA_TranslucentBackground)
            menu.setWindowFlags(menu.windowFlags() | Qt.FramelessWindowHint)
            menu.setStyleSheet("QMenu{background:rgba(0, 0, 0, 50%);}")# border-radius: 5px;}")

            submenus = []
            for menu_item in menu.children():
                if menu_item.children():
                    styleMenu(menu_item)


        menu = QtWidgets.QMenu(self)
        nodeMenu = menu.addMenu("Add Node")
        # XXX Probably shouldn't go all the way back to engine from here
        nodesSubMenu(nodeMenu, scene.engine.menu())
        menu.addSeparator()
        menu.addAction("Group Selected", groupSelectedNodes)
        menu.addAction("Collapse Selected", collapseSelectedNodes)
        styleMenu(menu)

        menu.exec_(self.mapToGlobal(self.mousePos))

    def _dragOutEdge(self, position):
        items = self.items(position) or []
        items = [i for i in items if isinstance(i, Plug)]
                
        if not items:
            return False
        item = items[0]

        if not self.newEdge:
            color = item.plug_color()
            self.newEdge = Edge(color=color)
            self.scene().addItem( self.newEdge )
        if item.plug_type() == Plug.Type.INPUT:
            self.newEdge.setTarget(item)
        elif item.plug_type() == Plug.Type.OUTPUT:
            self.newEdge.setSource(item)

        return True

    def _newEdge(self):
        pos  = self.mousePos
        item = self.itemAt(pos)
        
        if self.newEdge:
            source = self.newEdge.getSourcePlug()
            target = self.newEdge.getTargetPlug()

            self.scene().removeItem(self.newEdge)

            if isinstance(item, Plug):
                if item.plug_type() == Plug.Type.INPUT:
                     target = item
                elif item.plug_type() == Plug.Type.OUTPUT:
                     source = item

            if source and target:
                self.addEdgeSignal.emit(AddEdgePackage(edge=self.newEdge, source=source, target=target))
                self.newEdge.adjust()

                # XXX Bad that Im connectting the nodes in the engine!
                #     Should send a signal to the App. Let the application
                #     do what it wants with the engine (I would like the App to create a
                #     Command object that connects the nodes. that way, it is also undoable)
                scene = self.scene()
                scene.engine.connect(source.parent.label(), source.name, 
                                    target.parent.label(), target.name)
                

        self.newEdge = None


    # XXX Behavior should be to emit a signal with just the node id's that are selected
    #     let the calling App deal with the engine backend
    def _updateSelection(self):
        nodedata = None
        selectedItems = self.scene().selectedItems()
        for i in self.items():
            if i in selectedItems:
                i.setSelected(True)
                
                if isinstance(i, Node):
                    scene = self.scene()
                    nodedata = scene.engine.serialize(i.label())
            else:
                i.setSelected(False)

        # XXX Will only emit the signal for the last selected node
        if nodedata:
            self.nodeSelectedSignal.emit(NodeSelectedPackage(nodedata=nodedata))

    def _deleteSelectedItems(self):
        selecteditems = self.scene().selectedItems()
        # if the item has a parentItem (ie, it's in a group)
        # dont let it be deleted
        selecteditems = [item for item in selecteditems if not item.parentItem()]
        self.deleteItemsSignal.emit(DeleteItemsPackage(items=selecteditems))

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        elif event.key() == Qt.Key_E:
            selected_items = self.scene().selectedItems()
            print (len(selected_items))
            if not selected_items: 
                return
            item = selected_items[0]
            if not isinstance(item, Node):
                return
            node = item    
            self.computeNodeSignal.emit(ComputeNodePackage(node=node))

        #
        # Copy/Paste functionality needs to be emitted as Commands
        if (event.key() == Qt.Key_C):
            if ((event.modifiers() & Qt.ControlModifier)):
                self.copy_buffer = []
                self.copy_count = 0
                items = self.scene().selectedItems()
                for item in items:
                    if isinstance(item, Node):
                        self.copy_buffer.append(item)

        if (event.key() == Qt.Key_V):
            if ((event.modifiers() & Qt.ControlModifier)):
                self.copy_count = self.copy_count + 1
                for item in self.copy_buffer:
                    # Only the Node object item has a clone() 
                    # constructor currently
                    if isinstance(item, Node):
                        new_item = Node.clone(item)
                        pos = new_item.pos()
                        pos.setX(item.x()+(10*self.copy_count))
                        pos.setY(item.y()+(10*self.copy_count))
                        new_item.setPos(pos)
                        self.addNodeSignal.emit(AddNodePackage(node=new_item))                      

        if event.key() == Qt.Key_A:
            width = self.scene().width()
            height = self.scene().height()
            
            nodes = [item for item in self.scene().items() if isinstance(item, Node)]
            import math
            max = int(math.sqrt(len(nodes)))
            rows = cols = 0
            x,y = width/2, height/2
            for node in nodes:
                node.setPos(QtCore.QPointF(x,y))
                cols = cols + 1
                y = y + 100
                if cols > max:
                    cols = 0
                    y = height/2
                    x = x + 100

        if event.key() == Qt.Key_Delete:
            self._deleteSelectedItems()

        # allow key press events to propagae to other handlers
        event.setAccepted(False)
         
    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.setDragMode(QtWidgets.QGraphicsView.RubberBandDrag)

    def wheelEvent(self, event):
        delta = event.angleDelta().y()
        if delta > 0:
            self.scaleView(1.075)
        elif delta < 0:
            self.scaleView(0.925)

    def mouseMoveEvent(self, event):
        # TODO This function needs to be optimized. 
        #      it's causing performance issues
        mousePos = event.pos()
        buttons = event.buttons()

        ## XXX I could do this in the NetworkItem.hoverEnter/LeaveEvent()s
        ## BUt those event's dont fire if you click/drag onto an item
        #over_items = set(self.items(mousePos))
        #items = set(self.items())
        #for item in items:
        #    item.hover=item in over_items
        #    item.update()

        if buttons == Qt.LeftButton:
            if self.newEdge:
                self.newEdge.mousePos = self.mapToScene(mousePos)
                self.newEdge.adjust()

        return super(InteractiveNetworkView, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        self.mousePos = event.pos()

        button = event.button()
        if button == Qt.LeftButton:
            self._dragOutEdge(event.pos())

        super(InteractiveNetworkView, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        moved_items = {}
        for item in self.scene().selectedItems():
            if not isinstance(item, Node):
                continue
            if item.old_position != item.pos():
                moved_items[item] = (item.old_position, item.pos())
                item.old_position = item.pos()

        if moved_items:
            self.itemsMovedSignal.emit(ItemsMovedPackage(moved_items=moved_items))

        self.mousePos = event.pos()

        button = event.button()
        if button == Qt.RightButton:
            self._popupMenu()
        elif button == Qt.LeftButton: 
            self._newEdge()
            self._updateSelection()

        print("total number of scene items: %d" % len(self.scene().items()))
        super(InteractiveNetworkView, self).mouseReleaseEvent(event)


    # XXX HACK
    # this doesnt belong here, it really should be part of an 
    # "implementing application". Just putting here for now
    # out of laziness, to close without having to close child windows
    #def closeEvent(self, event):
    #    QtGui.QApplication.quit()

if __name__ == "__main__":
    import sys
    import NetworkEngine

    app = QtGui.QApplication(sys.argv)

    netengine = NetworkEngine.NetworkEngine()
    scene = NetworkScene(engine=netengine) #QtGui.QGraphicsScene()
    scene.setSceneRect(0, 0, 5000, 3000)

    network = InteractiveNetworkView()
    network.setScene(scene)
    network.move(50,50)
    
    minimap = MinimapWidget()
    minimap.minimapView.setScene(scene)
    minimap.setParent(network)
    minimap.move(20,20)
    minimap.show()

    parameterPane = ParameterWidget()
    parameterPane.resize(200,600)
    #parameterPane.setParent(network)
    parameterPane.move(1100,50)
    parameterPane.show()

    def selected_slot(package):
        parameterPane.setFromDict(package.nodedata)

    network.show()
    sys.exit(app.exec_())
        
