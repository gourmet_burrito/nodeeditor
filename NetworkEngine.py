import json

class NetworkEngine(object):
    def __init__(self):
        super(NetworkEngine, self).__init__()
     
    def menu(self):
        return {}

    def create(self, node_id):
        return None

    def serialize(self, node):
        return {}

import os
import sys
sys.path.append('C:/solidangle/mtoadeploy/2016/scripts')
sys.path.append('D:/projects/python/nodeeditor/solidangle/mtoadeploy/2016/scripts')
#sys.path.append('N:/proddev/dev/vicken/RandD/arnold/mtoadeploy/2015/scripts')


import arnold
import collections


def type_map(pentry):
    pname = arnold.AiParamGetName(pentry)
    ptype = arnold.AiParamGetType(pentry)
    pdefault = arnold.AiParamGetDefault(pentry)

    # type: (parameter reader, default value)
    type_map_ = {
        arnold.AI_TYPE_BYTE: (arnold.AiNodeSetByte, arnold.AiNodeGetByte, pdefault.contents.BYTE),
        arnold.AI_TYPE_INT: (arnold.AiNodeSetInt, arnold.AiNodeGetInt, pdefault.contents.INT),
        arnold.AI_TYPE_UINT: (arnold.AiNodeSetUInt, arnold.AiNodeGetUInt, pdefault.contents.UINT),
        arnold.AI_TYPE_BOOLEAN: (arnold.AiNodeSetBool, arnold.AiNodeGetBool, pdefault.contents.BOOL),
        arnold.AI_TYPE_FLOAT: (arnold.AiNodeSetFlt, arnold.AiNodeGetFlt, pdefault.contents.FLT),
        arnold.AI_TYPE_VECTOR: (arnold.AiNodeSetVec, arnold.AiNodeGetVec, [pdefault.contents.PNT.x, pdefault.contents.PNT.y, pdefault.contents.PNT.z]),
        arnold.AI_TYPE_POINT: (arnold.AiNodeSetPnt, arnold.AiNodeGetPnt, [pdefault.contents.PNT.x, pdefault.contents.PNT.y, pdefault.contents.PNT.z]),
        arnold.AI_TYPE_POINT2: (arnold.AiNodeSetPnt2,arnold.AiNodeGetPnt2, [pdefault.contents.PNT.x, pdefault.contents.PNT.y]),
        arnold.AI_TYPE_RGB: (arnold.AiNodeSetRGB,arnold.AiNodeGetRGB, [pdefault.contents.RGB.r, pdefault.contents.RGB.g, pdefault.contents.RGB.b]),
        arnold.AI_TYPE_RGBA: (arnold.AiNodeSetRGBA,arnold.AiNodeGetRGBA, [pdefault.contents.RGBA.r, pdefault.contents.RGBA.g, pdefault.contents.RGBA.b, pdefault.contents.RGBA.a]),
        #arnold.AI_TYPE_STRING: (arnold.AiNodeSetStr, arnold.AiNodeGetStr, "") #pdefault.contents.STR)
    }
    try: 
        type_map_.update({arnold.AI_TYPE_ARRAY: (arnold.AiNodeSetArray, arnold.AiNodeGetArray, pdefault.contents.ARRAY.contents)})
    except ValueError:
        pass

    return type_map_.get(ptype)


class AiContextManager(object):   
    def __enter__(self):
        arnold.AiBegin()

    def __exit__(self, exc_type, exc_value, traceback):
        arnold.AiEnd()

class ArnoldEncoder(json.JSONEncoder):
    def serialize_param(self, node, pentry):

        pname = arnold.AiParamGetName(pentry)
        ptype = arnold.AiParamGetType(pentry)

        mapping = type_map(pentry)

        if not mapping:
            print(pname, str(ptype), str(arnold.AI_TYPE_ARRAY))
            return None            

        set_func, get_func, default_value = mapping

        param = {"label":pname,
                "value":get_func(node, pname),
                "default":default_value,
                "type":ptype
        }

        return param 

    def default(self, item):
        if hasattr(item, 'contents') and isinstance(item.contents, arnold.AtNode):
            node = item
            # XXX I dont know how to query the parameters from the node
            #     directly (the the docs say there is AiNodeGetParams(AtNode)
            #     but I didnt get it working
            #     This isnt so ideal to go through the NodeEntry        
            node_entry = arnold.AiNodeGetNodeEntry(node)
            num_params = arnold.AiNodeEntryGetNumParams(node_entry)

            params = {}
            for p in range(num_params):
                pentry = arnold.AiNodeEntryGetParameter(node_entry, p)
                pname = arnold.AiParamGetName(pentry)           

                param = self.serialize_param(node, pentry)
                if param:
                    params[pname] = param

            outType = arnold.AiNodeEntryGetOutputType(node_entry)
            outName = arnold.AiParamGetTypeName(outType)
            node_name = arnold.AiNodeGetName(node)

            inputs = params.copy()
            outputs = {'output1':{'label':str(outName), 
                        'type':str(outType)}
                        }


            n = { 'label': node_name,
                  'image': './images/arnold.png',
                  'parameters': params,
                  'inputs': inputs,
                  'outputs': outputs
            }

            return n

        if isinstance(item, arnold.AtRGB):
            rgb = item
            return [rgb.r, rgb.b, rgb.b]

        elif isinstance(item, arnold.AtRGBA):
            rgba = item
            return [rgba.r, rgba.b, rgba.b, rgba.a]

#        elif isinstance(item. arnold.AtArray):
#            array = item
#            nelems = array.nelements
#            if nelems == 0:
#                return "(empty)"
#            elif nelems == 1:
#                if array.type == AI_TYPE_FLOAT:
#                return "%g" % AiArrayGetFlt(array, 0)
#            elif array.type == AI_TYPE_VECTOR:
#                vec = AiArrayGetVec(array, 0)
#                return "%g, %g, %g" % (vec.x, vec.y, vec.z)
#            elif array.type == AI_TYPE_POINT:
#                pnt = AiArrayGetPnt(array, 0)
#                return "%g, %g, %g" % (pnt.x, pnt.y, pnt.z)
#            elif array.type == AI_TYPE_RGB:
#                rgb = AiArrayGetRGB(array, 0)
#                return "%g, %g, %g" % (rgb.r, rgb.g, rgb.b)
#            elif array.type == AI_TYPE_RGBA:
#                rgba = AiArrayGetRGBA(array, 0)
#                return "%g, %g, %g" % (rgba.r, rgba.g, rgba.b, rgba.a)
#            elif array.type == AI_TYPE_POINTER:
#                ptr = cast(AiArrayGetPtr(array, 0), POINTER(AtNode))
#                return "%p" % ptr
#            elif array.type == AI_TYPE_NODE:
#                ptr = cast(AiArrayGetPtr(array, 0), POINTER(AtNode))
#                name = AiNodeGetName(ptr)
#                return str(name)
#            else:
#                return ""

        print("Error: unserializable type:", item,)
        return -1



class ArnoldNetworkEngine(NetworkEngine):
    '''
    I THINK the only env var required for plugins to load is the ARNOLD_PLUGIN_PATH. 
    Then the nodes can be loaded via the...i think the path that contains the mtd files.

    Here, I have mtoa working by setting these two env vars (KICK_SEARCHPATH is just searched in 
    python code. i ripped it off from Arnold's pykick.py)

    ARNOLD_PLUGIN_PATH=N:\proddev\dev\vicken\RandD\arnold\mtoadeploy\2015\plug-ins
    KICK_SEARCHPATH=N:\proddev\dev\vicken\RandD\arnold\mtoadeploy\2015\shaders
    '''
    @staticmethod
    def count_():
        i = 0
        while True:
            yield i
            i = i + 1

    def __init__(self):
        super(ArnoldNetworkEngine, self).__init__()
        #os.environ['KICK_SEARCHPATH'] = 'N:/proddev/dev/vicken/RandD/arnold/mtoadeploy/2015/shaders'
        os.environ['KICK_SEARCHPATH'] = 'C:/solidangle/mtoadeploy/2016/shaders'

        arnold.AiBegin()
        flags = arnold.AI_LOG_INFO|arnold.AI_LOG_WARNINGS|arnold.AI_LOG_ERRORS|arnold.AI_LOG_PROGRESS
        flags = arnold.AI_LOG_ALL
        arnold.AiMsgSetConsoleFlags(flags)

        for p in os.getenv('KICK_SEARCHPATH', '').split(os.pathsep):
            print("loading plugins from: ", p)
            arnold.AiLoadPlugins(p)

        self.gen = ArnoldNetworkEngine.count_()

    def __del__(self):
        arnold.AiEnd()

    def menu(self):    
        # XXX From collections module, use OrderedDict to make submenus sorted
        main_menu = {}

        it = arnold.AiUniverseGetNodeEntryIterator(arnold.AI_NODE_ALL)
        while not arnold.AiNodeEntryIteratorFinished(it):
            nentry   = arnold.AiNodeEntryIteratorGetNext(it)
            nodename = arnold.AiNodeEntryGetName(nentry)
            typename = arnold.AiNodeEntryGetTypeName(nentry)            
            filename = arnold.AiNodeEntryGetFilename(nentry)
            library = 'arnold'
            if filename:
                filename = filename.value
                basename = os.path.basename(filename)
                library = os.path.splitext(basename)[0]

                # XXX if python3
                library =  library.decode("utf-8")

            lib_menu = main_menu.setdefault(library, dict())
            type_items = lib_menu.setdefault(typename, list())
            type_items.append(nodename)
            type_items.sort() # XXX stupid but will do for now

        arnold.AiNodeEntryIteratorDestroy(it)

        return main_menu

    def serialize(self, node_name):
        ''' 
        Return a python dictionary version of the node's internal
        '''
        node = arnold.AiNodeLookUpByName(node_name)        
        node_data = json.loads(json.dumps(node, cls=ArnoldEncoder))

        return node_data

    def create(self, node_id):
        node = arnold.AiNode(str(node_id))
        if not node:
            print("ERROR: ArnoldNetworkEngine. Couldnt create node %s" % node_id)
            return

        # XXX python2
        #arnold.AiNodeSetStr(node, 'name', '%s%s' % (node_id, self.gen.next()))
        # XXX python 3
        arnold.AiNodeSetStr(node, 'name', '%s%s' % (node_id, next(self.gen)))


        node_name = arnold.AiNodeGetName(node)
        #arnold.AiNodeDestroy(node)
        n = arnold.AiNodeLookUpByName(node_name)

        node_data = self.serialize(node_name)
        return node_data

    def setParam(self, node_name, param_name, value):
        node = arnold.AiNodeLookUpByName(str(node_name))
        if not node:
            print("ERROR: ArnoldNetworkEngine. Couldnt find node %s" % node_id)
            return

        #arnold.AiNodeSetFlt(node, param_name, value)

        
        # XXX This whole block below is just to figure out the the param entry
        #     so that i know what type it is and which function to use to set it's value!
        #     If there is no better way in arnold's API then let's encapsulate this away 
        #     inside a private function to the class
        node_entry = arnold.AiNodeGetNodeEntry(node)
        pentry = None
        for p in range(arnold.AiNodeEntryGetNumParams(node_entry)):
            pentry = arnold.AiNodeEntryGetParameter(node_entry, p)
            pname = arnold.AiParamGetName(pentry)
            if pname == param_name:
                break
        if not pentry:
            return

        mapping = type_map(pentry)

        if not mapping: 
            return        
        
        set_func, get_func, default_value = mapping        
        set_func(node, param_name, value)

    def connect(self, source_node_name, source_plug_name, target_node_name, target_plug_name):
        source_node = arnold.AiNodeLookUpByName(source_node_name)
        target_node = arnold.AiNodeLookUpByName(target_node_name)
        # XXX equivalent to: arnold.AiNodeLinkOutput(source_node, "", target_node, target_plug_name);
        arnold.AiNodeLink(source_node, target_plug_name, target_node)

    def disconnect(self, node_name, plug_name):
        node = arnold.AiNodeLookUpByName(node_name)
        arnold.AiNodeUnlink(node, plug_name)

        #arnold.AiNodeSetPtr(target_node, target_plug, source_node)

    def to_file(self, filename):        
        arnold.AiASSWrite(filename, arnold.AI_NODE_ALL, False) # third arg False to keep file ascii




from PyQt5 import QtGui, QtCore, QtWidgets
import inspect
class QTNetworkEngine(object):
    def __init__(self):
        super(QTNetworkEngine, self).__init__()

        self.nodes = self.node_list()

        import collections
        self.nodes = collections.defaultdict(collections.defaultdict)
        for n in self.node_list():
            self.nodes[n] = collections.defaultdict(collections.defaultdict)

    def node_list(self):
        items = []
        for name in dir(QtCore):
            obj = getattr(QtCore, name)
            if inspect.isclass(obj) and issubclass(obj, QtCore.QObject):
                items.append(name)

        return items

    def node(self, name):
        if name not in self.nodes:
            return

        return getattr(QtCore, name)


        
