from PyQt5 import QtCore, QtGui
#from PyQt4.Qt import Qt

from NetworkItem import NetworkItem

#
# TODO:
# - change getters and setters to pythonic @property and @func.setter 
#   decorators and update the calling classes
#

class Edge(NetworkItem):
    def __init__(self, **kwargs):
        self.color = kwargs.get('color', (255,255,255))
        super(Edge, self).__init__()

        #self.setFlag(QtGui.QGraphicsItem.ItemStacksBehindParent)

        self.source = kwargs.get('source', None)
        self.target = kwargs.get('target', None)

        self.arrow_size = 5.0

        self.mousePos = None
        self.start = QtCore.QPointF()
        self.end = QtCore.QPointF()
        self.setZValue(0)

        self.percent = 0.5

        #self.timer = QtCore.QTimer()
        #self.timer.setInterval(10)
        
        #self.timer.timeout.connect(self.timeout)
        #self.percent = 0
        #self.point = 0#QtCore.QPointF()
        #self.timer.start()

    #def timeout(self):
    #    self.percent = (self.percent + 0.1) %1

        
    #    line = QtCore.QLineF(self.start, self.end)
    #    length = line.length()
    #    self.point = (self.point + 1)
    #    if self.point > length:
    #        self.point = 0

    #    self.update()

        self.shape = None

    def mouseReleaseEvent(self, event):
        super(Edge, self).mouseReleaseEvent(event)

    def mousePressEvent(self, event):
        super(Edge, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        super(Edge, self).mouseMoveEvent(event)

    def getTargetPlug(self):
        return self.target

    def getSourcePlug(self):
        return self.source

    def getSourceNode(self):
        return self.source.parent

    def getTargetNode(self):
        return self.target.parent

    def setSource(self, source):
        self.source = source
        
        if self.target:
            self.source.attachEdge(self)
            self.target.attachEdge(self)

    def setTarget(self, target):
        self.target = target

        if self.source:
            self.source.attachEdge(self)
            self.target.attachEdge(self)

    def setSelected(self, state):
        self.selected = state
        self.update()

    def adjust(self):
        if self.mousePos:
            self.start = self.mousePos
            self.end = self.mousePos

        if self.source:
            self.start = self.mapFromItem(self.source, self.source.boundingRect().center())

        if self.target:
            self.end = self.mapFromItem(self.target, self.target.boundingRect().center())

        # Prepares the item for a geometry change. Call this function before changing the 
        # bounding rect of an item to keep QGraphicsScene's index up to date.
        #
        # prepareGeometryChange() will call update() if this is necessary.

        self.prepareGeometryChange()

    def shape(self):
        if self.shape:
            return self.shape

        return super(Edge, self).shape()

    def boundingRect(self):
        # a little extra for the center arrow
        # should calculate this properly with trig
        penWidth = 1 
        extra = (self.arrow_size + penWidth)/2

        return QtCore.QRectF(self.start,
                QtCore.QSizeF(self.end.x() - self.start.x(),
                        self.end.y() - self.start.y())).normalized().adjusted(-extra, -extra, extra, extra)

    def paint(self, painter, option, widget):
        super(Edge, self).paint(painter, option, widget)
        painter.setClipRect(option.exposedRect)

        start = self.start
        end = self.end

        center  = QtCore.QPointF((start+end)/2)
        c1      = QtCore.QPointF(center.x(), start.y())
        c2      = QtCore.QPointF(center.x(), end.y())

        self.shape = QtGui.QPainterPath()
        self.shape.moveTo(start)
        self.shape.cubicTo(c1,c2,end)

        percent = self.percent#0.5
        #percent = self.shape.percentAtLength(self.point)
        center = self.shape.pointAtPercent(percent)

        import math
        angle_up = math.radians(self.shape.angleAtPercent(percent) + 60)
        cos = math.cos(angle_up)
        sin = math.sin(angle_up)

        self.shape.moveTo( center )
        self.shape.lineTo( center.x()-self.arrow_size*sin, center.y()-self.arrow_size*cos )

        angle_down = math.radians(self.shape.angleAtPercent(percent) - 60)
        cos = math.cos(angle_down)
        sin = math.sin(angle_down)
        
        self.shape.moveTo(center)
        self.shape.lineTo(center.x()+self.arrow_size*sin, center.y()+self.arrow_size*cos)

        r,g,b = self.color
        penClr = QtGui.QColor(r,g,b)

        if self.selected:
            penClr = QtCore.Qt.white

        pen = QtGui.QPen( penClr )

        pen.setWidthF(1.0)
        painter.setPen(pen)
        painter.drawPath(self.shape)

