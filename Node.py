from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from NetworkItem import NetworkItem
from Plug import Plug
from Edge import Edge

# TODO The creation of Plugs and the Node label Proxy widget cause
#      a lot of performance problems. Need to update such that the 
#      label is painted onto the node painter, instead of using a proxy
#
#      Then the Plugs class needs to have similar updates done
#      Or possibly eliminate plugs as sub-items completely. That 
#      would improve performance greatly

class NodeMovedPackage(object):
    def __init__(self, **kwargs):
        self.item = kwargs.get('item', None)
        #self.start = kwargs.get('start', None)
        #self.end = kwargs.get('end', None)

# QGraphicsViewItems don't inherit from QObject, so they cant emit signals
# So this notifier class is used to help out

class Notifier(QtCore.QObject):
    nodeMovedSignal = QtCore.pyqtSignal(NodeMovedPackage)

    def __init__(self, **kwargs):
        super(QtCore.QObject,self).__init__()
   
class Node(NetworkItem):

    def __init__(self, **kwargs):
        super(Node, self).__init__(**kwargs)
        self.id_ = kwargs.get("ID", None)

        self.inputs  = []
        self.outputs = []

        self.font_size = 11
        self.font = QtGui.QFont()
        self.font.setPointSize(self.font_size)
        self.fm = QtGui.QFontMetrics(self.font)
        self.incr = self.fm.height()

        self.offset = 20
        self.width  = 100
        self.height = 100

        self.old_position = self.pos()

        self._label = '' #QtGui.QLabel('')
        self._image = QtGui.QPixmap("")
        self._color = QtGui.QColor(25,25,25, 200)

        self.old_position = QtCore.QPointF()
        self.notifier = Notifier(item=self)
        self.nodeMovedSignal = self.notifier.nodeMovedSignal # an alias to get the signal conveniently

    def label(self):
        return self._label

    def _setLabel(self, text):
        self._label = text
        return

    def _setImage(self, img_path):
        import os
        if not os.path.isfile(img_path):
            return

        self._image = QtGui.QPixmap(img_path)

    def _addInput(self, plug):
        if plug in self.inputs:
            return
        rad = plug.radius
        self.inputs.append(plug)
        self.adjust()

        accum  = self.offset        
        x = -1*rad

        for plug in self.inputs:
            plug.setPos(QtCore.QPointF(x, accum))
            plug.setParentItem(self)
            accum = accum + self.incr

    def _addOutput(self, plug):
        if plug in self.outputs:
            return
        rad = plug.radius
        self.outputs.append(plug)
        self.adjust()

        accum = self.incr*len(self.inputs)
        accum = self.offset + accum

        x = self.width - rad

        for plug in self.outputs:
            plug.setPos(QtCore.QPointF(x, accum))
            plug.setParentItem(self)
            accum = accum + self.incr

    def adjust(self):
        def _height():
            inputs  = len(self.inputs)
            outputs = len(self.outputs)
      
            height = self.offset+(self.incr*(max(inputs+outputs,1)))
            return height

        def _width():
            img_width = self._image.scaledToHeight(self.offset, mode=Qt.SmoothTransformation).width()
            # XXX Python2
            #label_width = self.fm.width(QtCore.QString(self._label or ''))
            label_width = self.fm.width(self._label or '')
            header_width = img_width+label_width
            header_width = header_width + 3 + self.font_size/2 # mimicking placement in paint()

            longest = ""
            if self.inputs or self.outputs:
                for plug in self.inputs+self.outputs:
                    if self.fm.width(plug.name) > self.fm.width(longest):
                        longest = plug.name
                               
            plug_width = max(self.fm.width(longest)+10, 100)

            return max(header_width, plug_width)

        def updateEdges(func, *parms):
            edges = set()
            for plug in self.inputs + self.outputs:
                for edge in plug.edges:
                    if edge in edges:
                        continue
                    edges.add(edge)
                    func(*((edge,)+parms))

        self.width = _width()+10
        self.height = _height()+10

        self.prepareGeometryChange()
        
        updateEdges(Edge.adjust)
        updateEdges(Edge.setVisible, self.isVisible())
    
    def getInput(self, index):
        return self.inputs[index]

    def getOutput(self, index):
        return self.outputs[index]

    def boundingRect(self):
        width = self.width
        height = self.height

        bbox = QtCore.QRectF(0,0,width, height)
        bbox.adjust(-0.5,-0.5,0.5,0.5)
        return bbox
 
    def paint(self, painter, option, widget):
        super(Node, self).paint(painter, option, widget)
        painter.setClipRect(option.exposedRect)


        main_shape = QtGui.QPainterPath()
        width = self.width
        height = self.height
        radius = 5
        main_shape.addRoundedRect(0, 0, width, height, radius, radius)


        penClr = QtCore.Qt.darkGray
        if self.selected:
            penClr = QtCore.Qt.white
        
        col1 = QtGui.QColor(35,105,140)       
        col1.setAlpha(200)

        col2 = self._color
        #col2.setAlpha(200)

        gradient = QtGui.QLinearGradient(50, self.offset-10, 50, self.offset)
        gradient.setColorAt(0, col1)
        gradient.setColorAt(1, col2)

        painter.setBrush(QtGui.QBrush(gradient))
        painter.setPen(QtGui.QPen(penClr))
        painter.drawPath(main_shape)

        painter.setPen(QtGui.QPen(QtGui.QColor(200, 200, 200)))

        # the image of the ndoe
        img = self._image.scaledToHeight(self.offset, mode=Qt.SmoothTransformation)
        painter.drawPixmap(QtCore.QRect(3,0,self.offset, self.offset), img)

        # the label text for the node
        painter.setFont(self.font)
        x = img.width() + (self.font_size/2) 
        y = (self.font_size/2) + self.offset/2
        painter.drawText(x,y,self._label)

        for plug in self.inputs:
            plug_rect = plug.boundingRect() # QRect
            plug_pos = plug.pos() # QPoint
            x,y = plug_pos.x()+plug_rect.right(), plug_pos.y()+plug_rect.bottom()
            painter.drawText(x,y, plug.name)

        for plug in self.outputs:
            text_width = self.fm.width(plug.name)
            plug_rect = plug.boundingRect() # QRect
            plug_pos = plug.pos() # QPoint
            x = plug_pos.x()-text_width
            y = plug_pos.y()+plug_rect.bottom()
            
            painter.drawText(x,y, plug.name)


    def itemChange(self, change, value):
        if change == QtWidgets.QGraphicsItem.ItemPositionHasChanged:
            self.nodeMovedSignal.emit(NodeMovedPackage(itemChange=self))
            self.adjust()

        if change == QtWidgets.QGraphicsItem.ItemVisibleHasChanged:
            self.adjust()

        return super(Node, self).itemChange(change, value)

    def mouseMoveEvent(self, event):
        super(Node, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        super(Node, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        super(Node, self).mouseReleaseEvent(event)

    @classmethod
    def clone(cls, srcNode):
        '''
        Return a clone of the provided node
        '''
        node = cls(ID=srcNode.id_)                    
        
        node._label = srcNode._label        
        node._image = srcNode._image.copy(QtCore.QRect())

        node.setFlags(srcNode.flags())
                
        for srcPlug in srcNode.inputs:
            plug = Plug(parent=node, name=srcPlug.name, plug_type=Plug.Type.INPUT, color=srcPlug.color)
            plug.radius = srcPlug.radius
            plug.thickness=  srcPlug.thickness
            node._addInput(plug)

        for srcPlug in srcNode.outputs:
            plug = Plug(parent=node, name=srcPlug.name, plug_type=Plug.Type.OUTPUT, color=srcPlug.color)
            plug.radius = srcPlug.radius
            plug.thickness=  srcPlug.thickness
            node._addOutput(plug)

        return node

    @classmethod
    def createFromDict(cls, nodeId, nodeDict):
        '''
        Perhaps this constructor is better suited as part of a subclass
        '''

        node = cls(ID=nodeId)
        label = nodeDict.get("label", "")
        image = nodeDict.get("image", "")
        
        node._setLabel(label)
        node._setImage(image)

        inputs = nodeDict.get("inputs", {})
        for input_id, input_data in inputs.items():
            name = input_data["label"]
            datatype = input_data["type"]
            plug = Plug(parent=node, name=name, plug_type=Plug.Type.INPUT, color=Plug.Color.DATA)
            #plug.setFlags(QtGui.QGraphicsItem.ItemIgnoresTransformations)
            node._addInput(plug)

        outputs = nodeDict.get("outputs", {})
        for output_id, output_data in outputs.items():
            name = output_data["label"]
            datatype = output_data["type"]
            plug = Plug(parent=node, name=name, plug_type=Plug.Type.OUTPUT, color=Plug.Color.DATA)
            #plug.setFlags(QtGui.QGraphicsItem.ItemIgnoresTransformations)
            node._addOutput(plug)
        return node

    @classmethod
    def createFromQTObj(cls, nodeId, nodeObj):
        '''
        Perhaps this constructor is better suited as part of a subclass
        '''

        node = cls(ID=nodeId)
        label = nodeId
        image = "./images/qt.png"

        #node.obj = nodeObj()
        #node.obj.show()
        
        node._setLabel(label)
        node._setImage(image)

        plug = Plug(parent=node, name="process", plug_type=Plug.Type.INPUT, color=Plug.Color.DATA)
        plug.radius= (5.0/14)*node.incr
        plug.thickness = (2.0/14)*node.incr
        node._addInput(plug)

        from PyQt5 import QtCore
        for attr_name in dir(nodeObj):
            obj = getattr(nodeObj, attr_name)
            if isinstance(obj, QtCore.pyqtSignal):
                plug = Plug(parent=node, name=attr_name, plug_type=Plug.Type.OUTPUT, color=Plug.Color.DATA)
                plug.radius= (5.0/14)*node.incr
                plug.thickness = (2.0/14)*node.incr
                node._addOutput(plug)
        return node
