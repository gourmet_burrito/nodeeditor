from PyQt5 import QtCore, QtGui
from PyQt5.Qt import Qt

from NetworkItem import NetworkItem

# XXX Filling of the plug (when it's connected or disconnected
#     can be betetr handled by the painter fill mode, 
#     and the painter brush size
#
#     also, the position offset when makign hte ellipse seems dumb

class Plug(NetworkItem):
    class Type:
        INPUT  = 1
        OUTPUT = 2

    def __init__(self, **kwargs):
        self.parent = kwargs.get('parent')

        super(Plug, self).__init__(self.parent)

        self.plugtype = kwargs.get('plug_type', None)

        position   = kwargs.get('position', (0,0) )
        self.name  = kwargs.get('name')
        self.color = kwargs.get('color', Plug.Color.UNKNOWN )

        self.radius = 5
        self.thickness = 2

        self.edges = []

    def shape(self):
        shape = QtGui.QPainterPath()
        shape.addEllipse(self.boundingRect())
        return shape


    def boundingRect(self):
        r = self.radius
        d = 2*r
        bbox = QtCore.QRectF(0,0,d,d)
        bbox.adjust(-0.5,-0.5,0.5,0.5)
        return bbox

    def paint(self, painter, option, widget):
        super(Plug, self).paint(painter, option, widget)

        painter.setClipRect(option.exposedRect)
        penClr = QtCore.Qt.black
        pen = QtGui.QPen( penClr )
        pen.setWidthF(0.1)
        painter.setPen(pen)

        r,g,b = self.color
        painter.setBrush( QtGui.QColor(r,g,b) )

        r = self.radius
        t = r - self.thickness
        path = QtGui.QPainterPath()
        path.addEllipse(QtCore.QPointF(r,r), r, r)

        if not len(self.edges):
            path.addEllipse(QtCore.QPointF(r,r),t,t)

        painter.drawPath(path)
               
    def mouseMoveEvent(self, event):
        super(Plug, self).mouseMoveEvent(event)

    def mousePressEvent(self, event):
        super(Plug, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        super(Plug, self).mouseReleaseEvent(event)

    def plug_type(self):
        return self.plugtype

    def plug_color(self):
        return self.color

    def attachEdge(self, edge):
        if edge in self.edges:
            return

        self.edges.append(edge)
        self.update()

    def detachEdge(self, edge):
        if edge in self.edges:            
            self.edges.remove(edge)
        self.update()

    def connectedTo(self):
        connections = []
        for edge in self.edges:
            if edge.getSourcePlug() == self:
                connections.append(edge.getSourcePlug())
            else:
                connections.append(edge.getTargetPlug())
        return connections

