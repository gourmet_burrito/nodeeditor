from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

from NetworkView import NetworkView
from NetworkScene import NetworkScene
from NetworkEngine import NetworkEngine

class MinimapView(NetworkView):
    def __init__(self):
        super(MinimapView, self).__init__()
        self.refView = None

    def refViewBox(self):
        # XXX I think a pyton2 v python 3 difference with PyQT is causing the red rect 
        #     to "extend" when zoomed all the way out
        refView = self.refView
        matrix, isInvertible = refView.transform().inverted()
        hScroll, vScroll = refView.horizontalScrollBar().value(), refView.verticalScrollBar().value()
        visibleRect = matrix.mapRect(refView.viewport().rect())
        visibleRect.moveTopLeft(matrix.map(QtCore.QPoint(hScroll, vScroll)))
        return visibleRect

    def drawForeground(self, painter, rect):
        if not self.refView:
            return
        
        rect = self.refViewBox()
        painter.setPen(QtGui.QColor(200,50,50))
        painter.drawRect(rect)

class MinimapWidget(QtWidgets.QWidget):
    def __init__(self, **kwargs):
        super(MinimapWidget, self).__init__(**kwargs)
        self.minimapView = MinimapView()
        self.minimapView.setParent(self)
        self.minimapView.setDragMode(QtWidgets.QGraphicsView.NoDrag)
        
        self.fit()

    def fit(self):
        if not self.minimapView.scene():
            return 

        window_size = self.geometry().size()
        sceneRect = self.minimapView.sceneRect()
        self.minimapView.resize(window_size)
        self.minimapView.fitInView(sceneRect, Qt.KeepAspectRatio)

    def resizeEvent(self, event):
        self.fit()
        super(MinimapWidget, self).resizeEvent(event)
       
if __name__ == "__main__":
    import sys


    app = QtGui.QApplication(sys.argv)

    netengine = NetworkEngine()
    scene = MinimapScene(engine=netengine)
    scene.setSceneRect(0, 0, 7000, 5000)

    minimap = MinimapWidget()
    minimap.minimapView.setScene(scene)
    minimap.show()

    sys.exit(app.exec_())
